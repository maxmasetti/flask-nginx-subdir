from flask import Blueprint, jsonify, request, url_for

api = Blueprint('api', __name__)


@api.route("/ape/")
def hi1():
    return "<h1 style='color:green'>This is ape</h1>" + request.base_url


@api.route("/riccio/")
def hi2():
    return "<h1 style='color:red'>This is Riccio</h1>" + request.base_url


@api.route("/riccio/<par>/")
def hi3(par):
    return f"<h1 style='color:purple'>This is {par}</h1>" + request.base_url

