from flask import Flask, request

app = Flask(__name__)

from api import api
app.register_blueprint(api, url_prefix="/api")
#app.register_blueprint(api)

@app.route("/")
def hello():
    print(request.headers)
    return "<h1 style='color:blue'>Hello There!</h1>" + request.base_url


@app.route("/api/")
def hello1():
    return "<h1 style='color:green'>This is ROOT</h1>" + request.base_url


@app.route("/ciccio/")
def hello2():
    return "<h1 style='color:red'>This is Ciccio</h1>" + request.base_url


@app.route("/ciccio/<par>/")
def hello3():
    return f"<h1 style='color:purple'>This is {par}</h1>" + request.base_url


if __name__ == "__main__":
    app.run(host='0.0.0.0')

