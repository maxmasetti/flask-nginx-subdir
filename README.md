# Flask app and static files (Vue frontend) served by nginx + uwsgi

## Concept

Vue static files served on a subdirectory (docenti) by nginx loaded from app folder while Flask API served on a sub-subdirectory (docenti/api) forwarded from nginx to uwsgi but mounted with blueprint with url_prefix set to "/api".

A quite quirk solution is found but it seems it works.

Next step: install on a single dockerized tiangolo/flask-ngixn-uwsgi container and deployed by CI-CD by gitlab pipe.

## Debian test installation

```bash
cd api
python3 -m venv myprojectenv
source venv/bin/activate
pip install -r requirements.txt

cd ..
cp prova.service /etc/systemd/system/prova.service
cp nginx.cong /etc/nginx/sites-available/default

service prova enable
service prova start
service nginx restart
```

## Webliography

- [tutorial nginx+uwsgi](https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04)
- [nginx conf](https://uwsgi-docs.readthedocs.io/en/latest/Nginx.html#dynamic-apps)
- [nginx root and alias](https://stackoverflow.com/questions/10631933/nginx-static-file-serving-confusion-with-root-alias)

